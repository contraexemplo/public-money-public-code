# Cooperação internacional através do Software Livre

> As soluções em Software Livre estão ajudando governos a superarem diferentes desafios, desde governança democrática até prevenção de desastres naturais. Alguns projetos não são apenas disponibilizados, mas como também desenvolvidos internacionalmente. Projetos populares, tais como Consul, GNU Health, X-Road e CKAN ressaltam o potencial das licenças de Software Livre para cooperação através das fronteiras.

A cooperação entre nações através do Software Livre ajuda a estimular a inovação, elevar o desenvolvimento econômico, e assegurar valores de autonomia e sustentabilidade. Reusar e compartilhar software existente além das fronteiras economiza tempo e recursos valiosos, estimula a colaboração, e simplifica a intregração de dados entre organizações, administrações públicas e instituições. O Ministério Federal da Cooperação e Desenvolvimento alemão, por exemplo, encoraja o uso de padrões abertos e licenças de Software Livre em projetos que recebem financiamento seu pois isso pode abrir portas para cooperações futuras.

"Configurar cinco plataformas diferentes de participação cidadã em cada país parece contraproducente. Ainda assim, é comum casos em que diferentes ONGs e organizações que desenvolvem de forma cooperativa produzam plataformas similares porém competitivas. Para evitar esse tipo de duplicidade, descubra quais iniciativas tem sido produzidas por atores locais, ou outras organizações, e faça contato com eles." [1] - Ministério Federal da Cooperação e Desenvolvimento alemão.

Projetos de Software Livre iniciados e financiados por administrações públicas já oferecem funcionalidades variadas. Eles ilustram um alto nível de cooperação transnacional com soluções significativas disponibilizadas por cidadãos do mundo inteiro.

## X-Road

O X-Road fornece um meio para corporações do setor público e do privado conectarem sistemas de bancos de dados internacionalmente. A flexibilidade desse macrossistema é um benefício para cidadãos e funcionários públicos. A troca direta de dados dentro do X-Road permite às instituições economizarem tempo, recursos e custos, enquanto que sua estrutura distribuída assegura disponibilidade, integridade e confidencialidade para a informação trocada. O X-Road foi iniciado pelo governo da Estônia e está em contínuo funcionamento por 15 anos. Em 2017, por exemplo, o X-Road conectou uma infinidade de instituições, bases de dados e serviços, atendendo 563,3 milhões de requisições e economizando cerca de 800 anos de tempo de trabalho.

## CKAN

O CKAN (Rede Compreensiva de Arquivos de Conhecimento) fornece ferramentas para agilizar o processo de publicação, compartilhamento e busca de dados. Através de uma interface de gerenciamente de conteúdo, o serviço melhora a acessibilidade e a utilidade dos dados. Os usuários podem melhorar a facilidade de busca de seus dados e organizar catálogos com etiquetas especializadas. Por esses motivos que governos de 31 países adotaram o CKAN em suas políticas de dados abertos, permitindo aos cidadãos explorarem numerosos bancos de dados de todo o país com facilidade. Além desses países individualmente, o Portal de Dados Abertos da União Europeia também utiliza este software. Nele, qualquer um pode acessar resultados de pesquisas sobre países do bloco. O CKAN é um projeto da Open Knowledge Foundation, que mantém seu código fonte principal.

## GNU Health

O GNU Health oferece uma solução informacional para infraestruturas de sistemas públicos de saúde e para administração de centros médicos sociais. Em 2008, o projeto passou a apoiar a prevenção de doenças em áreas rurais e desde então evoluiu para um sistema de informação em larga escala de dados sobre saúde, contando com um time internacional de contribuidores. Ele tem sido adotado pela Universidade das Nações Unidas além de outras instituições ao redor do mundo. O GNU Health utiliza uma abordagem modular, com diferentes funcionalidades que podem ser adicionadas para atender às necessidades específicas de centros de saúde. A boa escalabilidade permite que ele seja utilizado em diferentes cenários por indivíduos e por organizações públicas de saúde.

## Consul

O Consul permite que os cidadãos participarem como formuladores de decisão dos governos de suas cidades, participarem de votações eletrônicas, apoiar projetos e criar petições baseadas em demandas. É um projeto de software criado especialmente para administrações municipais. Ele é usado e desenvolvido por mais de 90 governos nacionais e locais do mundo todo. Inicialmente desenvolvido pela Câmara Municipal de Madri, ele fornece uma plataforma de debates e propostas em formato de fórum, incluindo orçamentos e páginas cusotmizáveis para processos sobre legislação. O Consul é um Software Livre, portanto pode ser instalado por qualquer instituição governamental. Desenvolvedores podem se juntar ao projeto. Através da construção e da utilização do código, o software torna-se parte da comunidade.

## Mapa

O X-Road é usado na Estônia, Finlândia, no Azerbaijão, nas Ilhas Faroé, na Argentina e em El Salvador.

O CKAN é usado por governos de 31 países, incluindo a Alemanha, o Reino Unido, a Holanda, a Austrália, o Brasil e os Estados Unidos.

O GNU Health é usado no Brasil, na Espanha, Alemanha, Áustria, Argentina, no México, Peru, na Guatemala, em Honduras, Camarões, Jamaica e na República Dominicana.

Consul é usado na Espanha, França, Albânia, em Malta, na Eslovênia, no Brasil, Chile, na Bolívia, Costa Rica, no Peru, Equador, na Colômbia, Guatemala, no México e na Coreia do Sul.

***

## Notas de rodapé

[1] Ministério Federal da Cooperação e Desenvolvimento alemão, “Toolkit – Digitalisation in Development. Cooperation and International Cooperation in Education, Culture and Media”, 2016. pg. 91
