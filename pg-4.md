# O que é Software Livre?

> Os princípios do Software Livre são simples mas as formas de licenciamento e seus sinônimos acabam gerando confusões. Explicaremos os seus fundamentos.

O termo Software Livre foi criado em 1986 por Richard M. Stallmen. Software Livre se refere a liberdade, não ao preço. Ele garante aos seus usuários as 4 liberdades essenciais. A ausência de pelo menos uma dessas liberdades significa que um aplicativo é proprietário, portanto software não-livre.

## As quatro liberdades

### Usar

Software Livre pode ser usado para qualquer propósito e é livre de restrições como expiração de licenças ou limitações geográficas.

### Compartilhar

Software Livre pode ser copiado e compartilhado a um custo praticamente nulo.

### Estudar

O Software Livre, e seu código, pode ser estudado por qualquer pessoa, sem acordos de não-divulgação ou restrições similares.

### Melhorar

Software Livre pode ser modificado por qualquer pessoa, e suas melhorias podem ser compartilhadas publicamente.

## Licenças

As quatro liberdades são garantidas através de uma licença de software. A Free Software Foundation [1] e a Open Source Initiative [2] mantêm listas de licenças revisadas e aprovadas. Um aplicativo geralmente não deve ser considerada Software Livre se sua licença não aparecer nestas listas.

Há uma miríade de licenças com diferentes pontos focais. A escolha na verdade é uma questão estratégica e é aconselhado que se opte por uma das licenças mais amplamente utilizadas.

## Sinônimos

Ao longo do tempo, pessoas criaram rótulos adicionais para o Software Livre. [3] Geralmente, a motivação por trás desses termos é destacar diferentes características e evitar confusões.

### Software Livre

O termo original, criado em 1986

### Código Aberto

Formulado como uma campanha publicitária para o Software Livre em 1998.

### Libre Software

Criada para evitar a ambiguidade da palavra "free" no inglês [4], pegando emprestado a palavra usada no francês e no espanhol.

### FOSS/FLOSS

Abreviações de Free (Libre) and Open Source Software.

O grau de liberdade que um software específico oferece é sempre determinado pela sua licença, e não pelo seu rótulo. Ou seja, não confunda termos diferentes para as mesmas características.

## Notas de rodapé

[1] Mais informações sobre os diferentes termos e categorias de licença: https://fsfe.org/freesoftware/basics/comparison

[2] https://www.gnu.org/licenses/license-list.html

[3] https://opensource.org/licenses/category

[4] A palavra "free" no inglês pode significar tanto "grátis" quanto "livre" em vários contextos.