# Diferentes opções para publicar Software Livre 

> Contribuições do setor público para o Software Livre vêm em diferentes tamanhos e formatos. Dr. Matthias Stürmer, chefe do Centro de Pesquisa para Sustentabilidade Digital na Universidade de Bern, fornece argumentos de por que mesmo pequenas contribuições podem ter um grande impacto.

## 1. Resolução de *bugs* e melhoramentos de funcionalidades

Se uma agência está usando Software Livre como o MariaDB (um banco de dados) ou Angular (um *framework* da linguagem JavaScript), é essencial que os engenheiros de software internos liberem partes de software de vez em quando. Desenvolvedores que usam Software Livre podem consertar um *bug* ou adicionar algumas novas funcionalidades. Se eles mantiverem os *bugs* resolvidos e os código das funcionalidades apenas para si, o *bug* aparecerá novamente na próxima versão e a nova funcionalidade não poderá ser melhorada. É, portanto, de grande interesse da organização pública contribuir de volta com melhorias, por menores que sejam, ao repositório principal de desenvolvimento da solução de Software Livre. Se a melhoria for aceita, a nova versão já incluirá a resolução do *bug* e a nova funcionalidade, levando a uma maior velocidade de desenvolvimento e menos esforços redundantes.

## 2. Financiamento coletivo do desenvolvimento dos principais Softwares Livres

Em várias ocasiões, agências governamentais financiaram coletivamente o desenvolvimento de determinadas extensões de soluções de Software Livre já existentes. Por exemplo, swisstopo ajudou no financiamento do desenvolvimento da versão 3 do OpenLayes (um *framework* de mapas para web) junto com outros escritórios de topografia europeus. [1] Juntar dinheiro e então contratar fornecedores de serviços de Software Livre para melhorar aplicativos de Software Livre já existentes, ao invés de começar novos projetos, pode melhorar a qualidade do código e diminuir os gastos através de custeamentos compartilhados.

## 3. Lançar novos projetos de Software Livre

Começar um novo projeto em Software Livre (tal como o OpenJustitia da Corte Federal suíça, ou o portal geográfico da Swisstopo) através da liberação do código-fonte completo do produto de software é um investimento a longo prazo. São necessários recursos para preparar e liberar o código-fonte, a coordenação com a comunidade e possivelmente fundar uma associação sem fins lucrativos para controlar o código-fonte. Entretanto, se a criação da comunidade for bem sucedida, o software também será melhorado por outras agências, levando a uma solução mais ampla e reduzindo custos de desenvolvimento a longo prazo. Adicionalmente, através da criação de uma base de usuários ampla, o mercado de fornecedores de serviços em Software Livre cresce, diminuindo a dependência de fornecedores externos.

## > 
Esses três casos representam as diferentes formas que governos podem publicar Software Livre. O código-fonte resultante portanto se torna um bem público [2]: por definição, é não-exclusivo e não-competidor. Publicar software financiado por dinheiro público portanto faz sentido, visto que agências públicas devem investir em bens públicos para maximizar o seu benefício para a sociedade, como acontece por exemplo em seu suporte para pesquisa básica ou na promoção de proteções ambientais.

## Notas de rodapé

[1] http://www.ossdirectory.com/che/oss-top-news/single/article/institutionelles-crowdfunding-fuer-open-source-entwicklung-von-swisstopo

[2] https://link.springer.com/article/10.1007/s11625-016-0412-2