# Dinheiro Público Código Público

Este repositório contém a tradução para português brasileiro da brochura "Modernizando a Infraestrutura Pública com Software Livre" da campanha "Public Money Public Code", criada e coordenada pela Free Software Foundation Europe.

# Índice - Modernizando a Infraestrutura Pública com Software Livre

### [Contracapa](../pg-2.md)

### [Editorial 3](../pg-3.md)
por Matthias Kirschner, Presidente da FSFE

### [O que é software livre? 4](../pg-4.md)

### [Utilizando Software Livre para democratizar cidades inteligentes 6](../pg-6-7-8.md)
Entrevista com Francesca Bria, CTIO da Câmara Municipal de Barcelona

### [Os custos do aprisionamento tecnológico 8](../pg-8.md)

### [Campeões ocultos 9](../pg-9.md)

### [O impacto do Software Livre na concorrência 10](../pg-10-11.md)
por Prof. Simon Schulari, especialista em leis da concorrência

### [10 Mitos sobre Software Livre 12](../pg-12-13.md)

### [Criando negócios e sentido econômico com Software Livre 14](../pg-14-15.md)
por Cedric Thomar, CEO da OW2

### [Infográfico: Modernize sua TI 16](../pg-16-17.md)

### [Lições da abertura de software na Suíça 18](../pg-18-19.md)
por Dr. Marrihas Strmer, Centro de Pesquisa para a Sustentabilidade Digital

### [Diferentes opções para publicar Software Livre 20](../pg-20.md)

### [A caixa-preta dos softwares eleitorais 21](../pg-21.md)
Entrevista com Constanze Kurz, porta-voz do CCC

### [Uma nova abordagem para segurança em TI 22](../pg-22-23.md)
por Lori Roussey e Fernanda G. Weiden, especialistas em cybersegurança

### [Cooperação internacional através do Software Livre 24](../pg-24-25.md)

### [Projetos da União Europeia e políticas de apoio ao uso de Software Livre 26](../pg-26-27.md)

### [Reprogramando a Lei de Licitações 28](../pg-28.md)

### [Como licitar Software Livre 29](../pg-29.md)
por Basanta E.P. Thapa, Centro de Competências para TI Pública, Instituto Fraunhofer

### [Primeiros passos para apoiar Software Livre 30](../pg-30.md)

### [Última página](../pg-31.md)