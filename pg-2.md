Publicado pela Free Software Foundation Europe (FSFE)
Berlim, Janeiro de 2019
Identificador de registro de transparência da União Europeia: 33882407107-76
www.fsfe.org

Responsável perante a Lei Europeia de Imprensa:
Matthias Kirschner / FSFE e.V.
Schönhauser Allee 6/7
10119 Berlim
Alemanha

Contribuições de: Erik Alberts, Alexandra Busch, Matthias Kirschner, Max Mehl, Katharina Nocum, George Brooke-Smith, George Brooke‐Smith

Os artigos com contribuições de Francesca Bria (pág. 6) e Constanze Kurz (pág. 21) foram extraídos de entrevistas mais longas. Você pode lê-las integralmente em fsfe.org.

Editado por: Carol McGuigam, Jennifer Neal

Projetado por: Markus Meier 

Conteúdos deste relatório podem ser citados ou reproduzidos desde que a fonte da informação seja citada. Todos os conteúdos são, exceto se explicitamente fiyo o contrário, licenciados sob a licença Creative Commons 4.0, cláusulas BY-SA.

Créditos fotográficos:
págs. 1, 11, 23: Vídeo da campanha Dinheiro Público, Código Público. CC BY 4.0 de www.fsfe.org e motionensemble.de.

pág. 7: Cúpula Global 2018 de Inteligência Artificial para o Bem (AI for Good Global Summit). CC BY 2.0 por ITU Pictures.

pág. 15: Retrato de Cedric Thomas. Todos os direitos reservados.

pág. 15: Crepúsculo em "Lá Défense" - Paris. CC BY 2.0 por Gael Varoquaux.

pág. 21: Retrospectiva de fim de ano do Chaos Computer Club no 30º Congresso Chaos Communication em Hamburgo, 2013. CC BY-SA 4.0 por Wikipédia/Tobias Klenze.

Informações de licença:

https://creativecommons.org/licenses/by/2.0/deed.pt_BR

https://creativecommons.org/licenses/by/4.0/deed.pt_BR

https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR

Agradecimentos:
Nós gostaríamos de agradecer aos membros da equipe de campanha da FSFE pelas discussões e comentários inspiradores nos rascunhos dos textos. Agradecemos a Cedric Thomas, Prof. Dr. Simon Schulari, Dr. Matthias Stürmer, Basanta Thapa, Fernanda G. Weiden e Lori Roussey pelas suas maravilhosas contribuições. Somos gratos a Alexander Lehman e Lena Schall por produzir o vídeo da campanha Dinheiro Público Código Público e fornecerem os elementos gráficos para esta publicação. Agradecemos a Ura Design pelo seu apoio em visualizar os fatos mais importantes sobre o Software Livre. Agradecemos a nossos doadores, apoiadores e especialmente ao Digital Rights Fund pelo apoio financeiro a esta publicação.
