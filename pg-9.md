# Campeões ocultos

> Ao pensar no bem público, a maioria das pessoas pensa em ruas, escolas ou hospitais. Progressivamente, mais e mais administrações públicas agora também pensam em software e, sendo mais preciso, em Software Livre.

A lista de projetos de software financiados por dinheiro público tornando os seus códigos-fonte disponíveis publicamente e compartilhando-os com outras instituições cresce bastante dia após dia. Por exemplo, A Rede Nacional de Bibliotecas Alemãs (GBV) provê uma solução em Software Livre que é usada amplamente por bibliotecas na Alemanha. [1] O Estado de Luxemburgo oferece um registro eletrônico de dados de saúde utilizado por muitos médicos e clínicas. [2] O Ministério do Interior holandês publica boa parte do código-fonte do software do banco de dados de registro civil (BRP).[3] O Ministério das Finanças checo disponibiliza uma visualização em tempo real dos planos orçamentários das instituições. [4] Algumas soluções em Software Livre têm sido inclusive utilizadas internacionalmente. O serviço Nacional de Levantamento Topográfico finlandês desenvolveu o Oskari, um software para visualizar e analisar dados espaciais e estatísticos. O Oskari inclui funcionalidades que canalizam o feedback dos cidadãos quanto aos novos projetos de infraestrutura, fornece serviços de informação em tempo real e mostra áreas apropriadas para pesca. [5] Isso tem convencido o portal Geográfico Nacional islandês e o Serviço Nacional de Levantamento Topográfico da Moldávia a também utilizarem o Oskari.

Instituições que não consideram a possibilidade de publicar seus softwares estão deixando passar oportunidades importantes. Se um código potencialmente reutilizável está escondido de outros agentes governamentais, isso pode resultar em projetos de software redundantes e consequentemente maiores custos para as instituições e os contribuintes. Respaldadas por experiências positivas, as administrações públicas estão percebendo que compartilhar o código-fonte de seus projetos está totalmente de acordo com os seus próprios interesses. Centenas de agentes governamentais têm uma conta no GitHub [6], uma plataforma privada para compartilhamento de código, e alguns países até mantém os seus próprios repositórios públicos de código.

A mudança atualmente em curso no setor público não é meramente uma questão de quantidade. É também uma iniciativa que propõe melhor governança e mais transparência para os serviços governamentais. Têm sido comprovado que a transparência de software cria confiança na infraestrutura digital do governo, especialmente em áreas sensíveis. As políticas de Software Livre permitem verificações de segurança por agentes independentes. Um aplicativo de mensagens instantâneas criptografadas oferecido pela Agência de Cybersegurança Nacional francesa é baseado em dois projetos de Software Livre: Matrix e Riot. O projeto de Software Livre OSiP (Online‐Sicherheitsüberprüfung, ou Verificação de Segurança Online) é utilizado nas checagens de segurança em aeroportos alemães. [7] O código-fonte do ProZorro, software ucraniano vencedor de prêmio de transparência em processos de licitação, pode ser verificado online. [8] O capítulo ucraniano da ONG Transparência Internacional apoiou esta decisão. [9] Ainda mais importante que os benefícios econômicos do Software Livre está a conquista do bem mais valioso numa democracia: a confiança dos cidadãos na infraestrutura estatal. Quanto mais as infraestruturas dos Estados modernos dependerem da TI, mais críticos se tornarão esses argumentos.

***

## Destaque
> Centenas de agentes governamentais têm uma conta no GitHub

***

## Detalhe lateral

### OGPtoolbox

O software "Open Government Toolbox" (Kit de Ferramentas de Governo Aberto) agrupa mais de 1401 ferramentas (Software Livre em sua maioria) desenvolvidas por mais de 590 organizações. De visualização de dados até ferramentas voltadas à participação cidadã, e aplicações para iniciativas urbanas locais – o espectro dessa impressionante coleção mostra o potencial do Software Livre em conjunto com o uso de dados abertos. http://ogptoolbox.org

***

## Notas de rodapé
[1] https://github.com/gbv

[2] https://joinup.ec.europa.eu/community/osor/news/luxembourg-open-source-health-records-system-gains-foothold

[3] https://github.com/MinBZK

[4] https://github.com/otevrena-data-mfcr

[5] http://www.oskari.org

[6] https://government.github.com/community

[7] https://www.wirtschaft.nrw/onlinesicherheitspruefung-osip

[8] https://openprocurement.io

[9] https://ti-ukraine.org/en/news/prozorro-sale-wins-global-anti-corruption-challenge