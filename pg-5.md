# Índice

## 6 Como digitalizar a administração pública sem perder o controle?

## 10 O setor público tem permissão para liberar os seus próprios códigos publicamente?

## 18 Como a sustentabilidade digital funciona na prática?

## 22 Como a abertura melhora a segurança em TI?

## 29 Como podemos modernizar as licitações públicas?

### Editorial 3
por Matthias Kirschner, Presidente da FSFE

### O que é software livre? 4

### Utilizando Software Livre para democratizar cidades inteligentes 6
Entrevista com Francesca Bria, CTIO da Câmara Municipal de Barcelona

### Os custos do aprisionamento tecnológico 8

### Campeões ocultos 9

### O impacto do Software Livre na concorrência 10
por Prof. Simon Schulari, especialista em leis da concorrência

### 10 Mitos sobre Software Livre 12

### Criando negócios e sentido econômico com Software Livre 14
por Cedric Thomar, CEO da OW2

### Infográfico: Modernize sua TI 16

### Lições da abertura de software na Suíça 18
por Dr. Marrihas Strmer, Centro de Pesquisa para a Sustentabilidade Digital

### Diferentes opções para publicar Software Livre 20

### A caixa-preta dos softwares eleitorais 21
Entrevista com Constanze Kurz, porta-voz do CCC

### Uma nova abordagem para segurança em TI 22
por Lori Roussey e Fernanda G. Weiden, especialistas em cybersegurança

### Cooperação internacional através do Software Livre 24

### Projetos da União Europeia e políticas de apoio ao uso de Software Livre 26

### Reprogramando a Lei de Licitações 28

### Como licitar Software Livre 29
por Basanta E.P. Thapa, Centro de Competências para TI Pública, Instituto Fraunhofer

### Primeiros passos para apoiar Software Livre 30