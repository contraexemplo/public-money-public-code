Matthias Kirschner

Presidente da Free Software Foundation Europe

# Caro leitor,

Hoje, as tecnologias digitais são um elemento crucial na infraestrutura de Estados modernos. Isso desafia administrações públicas, levanta novas questões referentes a controle, segurança, eficiência, distribuição de poder, e transparência das instituições.

A Free Software Foundation Europe (FSFE) tem trabalhado como uma instituição de caridade desde 2001 empoderando usuários para controlar a tecnologia. Acreditamos que precisamos de tecnologias que deem poder aos usuários ao invés de restringir suas liberdades. O Software Livre dá a todos – indivíduos, empresas, organizações e administração pública – os direitos de usar, estudar, compartilhar e melhorar o software. Para a adminstração pública, Software Livre significa mais sustentabilidade, devido ao reuso de código de softwares existentes e aos benefícios do compartilhamento de código e de custos com outras instituições. Para empresas, sociedade civil e cidadãos, políticas inovadoras de licenciamento significam mais opções de escolha, transparência, competição e eficiência de custos.

O Software Livre na administração pública não é uma tendência de curto prazo. Os últimos anos têm demonstrado mudanças significativas de opinião de administrações públicas sobre aquisições de TI, favorecendo cada vez mais uma abordagem estratégica e de longo prazo. Mais e mais gestores públicos estão preocupados com os custos e perigos a longo prazo em virtude do aprisionamento tecnológico (Vendor Lock-In). Estratégias bem-sucedidas contra esse fenômeno, de comprovado funcionamento na prática, sustentam-se em grande parta em padrões abertos e licenças de Software Livre. Novas políticas de licitação ajudam a minimizar dependências e a baixar custos através de ofertas competitivas de Softwares Livres. Um crescente número de países têm implementado planos ou legislações que viabilizam o uso de licenças de Software Livre no setor público. Hoje, até mesmo projetos governamentais de TI de grande escala são publicados regularmente sob licenças de Software Livre.

A publicação desta brochura é uma resposta ao crescente número de solicitações do setor público enviadas a nós da FSFE. Essa coleção de artigos, entrevistas e informações básicas responde às questões mais comuns quanto à implementação de Software Livre no setor público. As páginas seguintes contêm casos de uso, informações de contexto e conselhos especializados relevantes para a modernização da infraestrutura pública. Como um cientista formado em administração pública, eu espero que este material contribua para a modernização da infraestrutura de TI na administração pública, e consequentemente proporcione melhores serviços para os cidadãos.

Atenciosamente,

Matthias Kirschner