# Os custos do aprisionamento tecnológico

Funcionalidades convincentes, bom serviço e confiança no fornecedor atual são motivos positivos pelos quais instituições devem evitar fazer mudanças em sua infraestrutura digital. O mais importante e geralmente subestimado motivo negativo para a renovação de um contrato com um fornecedor específico é o aprisionamento tecnológico.

O aprisionamento tecnológico torna os clientes dependentes de um único fornecedor. Isso cria obstáculos artificiais ao aumentar significativamente os custos e o esforço envolvido numa migração para outro fornecedor. Esse aprisionamento pode ser causado por obstáculos legais como cláusulas contratuais, dependência indispensável de outro software, licenças proprietárias, bem como padrões fechados ou obscuros que causam incompatibilidades. [1]

Nas administrações públicas há muitos casos de aprisionamento tecnológico. Por exemplo, formatos de arquivos de documentos que são legíveis apenas por um produto específico, conteúdos de banco de dados não conversíveis para o formato de um fornecedor concorrente, ou a obrigação da aquisição de uma atualização superfaturada de software para que seja possível acessar um arquivo e para receber atualizações de correção. Ao longo dos anos, muitas instituições têm gastado grandes quantias de recursos financeiros em sistemas desatualizados apenas para evitar os custos de migração causados pelo aprisionamento tecnológico.

Com a terceirização da prestação de serviços e de armazenamento através de provedores de nuvem se tornando cada vez mais proeminentes, o problema do aprisionamento tecnológico está crescendo. O controle e o conhecimento das tecnologias implantadas está sendo reduzido [2] enquanto que os custos podem facilmente explodir devido ao conhecimento global limitado. Quanto mais profunda for a integração de um departamento a um ambiente específico, mais difícil será a migração para soluções oferecidas por outros fornecedores.

Obviamente, clientes não escolhem conscientemente se tornarem presos a um determinado fornecedor, e geralmente não estão atentos a essa ameaça. Mas existem algumas formas de se evitar a chegar nessa situação:

> Observe o mercado antes de adquirir um produto e leve em conta tanto os custos de entrada quanto os de saída.

> Certifique-se de que os dados poderão ser migrados para fornecedores alternativos sem custos imprevisíveis.

> Use produtos que apoiam padrões abertos [3] independentes e interoperáveis com softwares alternativos.

> Use Softwares Livres que permitam a contratação de terceiros para aprimorar e corrigir o software.

Produtos de Software Livre que utilizam Padrões Abertos ajudam a prevenir a migrações de custos ao permitir melhorias incrementais e suporte independente. Eles proporcionam flexibilidade em um mundo digital em constante mudança.

## Notas de rodapé

[1] Mackintosh S. 2018, An Open Digital Approach for the NHS

[2] McKendrick J. 2011, Cloud Computing’s Vendor Lock‐In Problem: Why the Industry is Taking a Step Backward. https://www.forbes.com/sites/joemckendrick/2011/11/20/cloud-computings-vendor-lock-in-problem-why-the-industry-is-taking-a-step-backwards

[3] https://fsfe.org/activities/os